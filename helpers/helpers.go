package helpers

import (
	"log"
	"strings"
	"time"
)

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

// amsLocation refers to the Dutch time zone / location
var amsLocation *time.Location

func init() {
	var err error
	if amsLocation, err = time.LoadLocation("Europe/Amsterdam"); err != nil {
		log.Fatalf("error getting time zone: %v", err)
	}
}

// FormatTime formats the specified time in the Dutch time zone and language
func FormatTime(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006 15:04"))
}

// FormatDate formats the specified time in the Dutch time zone and language
func FormatDate(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006"))
}
