package backend

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

var baseURL = os.Getenv("BACKEND_ENDPOINT")

func init() {
	// Set fallback value for the BACKEND_ENDPOINT environment variable
	if baseURL == "" {
		baseURL = "http://localhost:8080"
	}
}

// Request performs a simple http request to the given path and unmarshals the result to the specified pointer
func Request(method string, path string, data interface{}, v interface{}) (err error) {
	client := http.Client{}
	var req *http.Request

	var jsonData []byte
	if data != nil {
		if jsonData, err = json.Marshal(data); err != nil {
			return
		}
	}

	if req, err = http.NewRequest(method, baseURL+path, bytes.NewBuffer(jsonData)); err != nil {
		return
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	// Do the request
	var resp *http.Response
	if resp, err = client.Do(req); err != nil {
		return
	}

	if resp.StatusCode != 200 {
		body, _ := io.ReadAll(resp.Body)
		err = fmt.Errorf("response returned with error code %v. response body: %v", resp.StatusCode, string(body))
		return
	}

	// Read the response body and possible error
	var result []byte
	if result, err = io.ReadAll(resp.Body); err != nil {
		return
	}

	// If v is set, unmarshal the response to v (which should be a pointer)
	if v != nil {
		err = json.Unmarshal(result, v)
	}

	return
}
