package backend

import (
	"time"

	"github.com/google/uuid"
)

type Person struct {
	ID  uuid.UUID `json:"id"`
	BSN string    `json:"bsn"`
}

type Vehicle struct {
	ID           uuid.UUID      `json:"id"`
	LicensePlate string         `json:"licensePlate,omitempty"`
	Brand        string         `json:"brand,omitempty"`
	Owners       []PrivateOwner `json:"owners,omitempty"`
}

type PrivateOwner struct {
	PersonID   uuid.UUID  `json:"personID"`
	AssignedAt *time.Time `json:"assigned_at"`
}

type Pagination struct {
	FirstID *uuid.UUID `json:"firstId"`
	LastID  *uuid.UUID `json:"lastId"`
	PerPage int        `json:"perPage"`
}

type Response[T any] struct {
	Data       []T         `json:"data"`
	Pagination *Pagination `json:"pagination"`
}
