package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"text/template"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/google/uuid"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-frontend/backend"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-frontend/helpers"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Vehicles
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the vehicles from the backend

		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(backend.Response[backend.Vehicle])
		if err := backend.Request("GET", fmt.Sprintf("/vehicles?%s", query.Encode()), nil, &response); err != nil {
			return fmt.Errorf("error fetching vehicles: %v", err)
		}

		return c.Render("index", fiber.Map{"vehicles": response.Data, "pagination": response.Pagination})
	})

	vehicles := app.Group("/vehicles")

	vehicles.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the vehicle from the backend
		var vehicle backend.Vehicle
		if err := backend.Request("GET", fmt.Sprintf("/vehicles/%s", c.Params("id")), nil, &vehicle); err != nil {
			return fmt.Errorf("error fetching vehicle: %v", err)
		}

		// Fetch the BSN for each owner. IMPROVE: change the backend to include this information in the request above
		var people []backend.Person
		if err := backend.Request("GET", "/people", nil, &people); err != nil {
			return fmt.Errorf("error fetching people: %v", err)
		}

		ownerBSNs := make(map[uuid.UUID]string)
		for _, owner := range vehicle.Owners {
			for _, person := range people {
				if owner.PersonID == person.ID {
					ownerBSNs[person.ID] = person.BSN
					break
				}
			}
		}

		return c.Render("vehicle-details", fiber.Map{
			"vehicle":   vehicle,
			"ownerBSNs": ownerBSNs,
		})
	})

	// People
	people := app.Group("/people")

	people.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the backend
		var people []backend.Person
		if err := backend.Request("GET", "/people", nil, &people); err != nil {
			return fmt.Errorf("error fetching people: %v", err)
		}

		return c.Render("person-index", fiber.Map{"people": people})
	})

	app.Get("/people/:id", func(c *fiber.Ctx) error {
		// Fetch the person's BSN from the backend by fetching all people and selecting the requested one. IMPROVE: improve this in the backend
		var people []backend.Person
		if err := backend.Request("GET", "/people", nil, &people); err != nil {
			return fmt.Errorf("error fetching person: %v", err)
		}

		var person backend.Person
		var err error
		if person.ID, err = uuid.Parse(c.Params("id")); err != nil {
			return err
		}

		for _, p := range people {
			if p.ID == person.ID {
				person = p
				break
			}
		}

		// Fetch the vehicles from the backend
		var vehicles []backend.Vehicle
		if err := backend.Request("GET", fmt.Sprintf("/people/%s/vehicles", person.ID), nil, &vehicles); err != nil {
			return fmt.Errorf("error fetching vehicle: %v", err)
		}

		return c.Render("person-details", fiber.Map{
			"person":   person,
			"vehicles": vehicles,
		})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
